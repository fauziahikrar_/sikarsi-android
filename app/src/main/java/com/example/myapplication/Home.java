package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

public class Home extends AppCompatActivity implements View.OnClickListener {

    private CardView laporan,riwayat,kontak,bantuan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        laporan = (CardView) findViewById(R.id.laporan);
        riwayat = (CardView) findViewById(R.id.riwayat);
        kontak = (CardView) findViewById(R.id.kontak);
        bantuan = (CardView) findViewById(R.id.bantuan);


        laporan.setOnClickListener(this);
        riwayat.setOnClickListener(this);
        kontak.setOnClickListener(this);
        bantuan.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i;

        switch (v.getId()){
            case R.id.laporan : i = new Intent(this, com.example.myapplication.laporan.class); startActivity(i); break;
            case R.id.riwayat : i = new Intent(this, com.example.myapplication.riwayat.class); startActivity(i); break;
            case R.id.kontak : i = new Intent(this, com.example.myapplication.kontak.class); startActivity(i); break;
            case R.id.bantuan : i = new Intent(this, com.example.myapplication.bantuan.class); startActivity(i); break;
            default:break;
        }

    }
}
