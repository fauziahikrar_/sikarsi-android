package com.example.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

public class laporan extends AppCompatActivity {
    ElegantNumberButton btn;
    Spinner spinnerminggu,spinnerbulan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarLaporan);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.judulLaporan);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String items_spinnerminggu[] = {"1","2","3","4","5"};
        MaterialBetterSpinner spinnerminggu = (MaterialBetterSpinner) findViewById(R.id.spinnerminggu);
        ArrayAdapter<String> adapter_spinnerminggu = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item, items_spinnerminggu);
        spinnerminggu.setAdapter(adapter_spinnerminggu);

        String items_spinnerbulan[] = {"Januari", "Februari","Maret","April","Mei","Juni","Juli","Agustus",
                                        "September","Oktober","November","Desember"};
        MaterialBetterSpinner spinnerbulan = (MaterialBetterSpinner) findViewById(R.id.spinnerbulan);
        ArrayAdapter<String> adapter_spinnerbulan = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item, items_spinnerbulan);
        spinnerbulan.setAdapter(adapter_spinnerbulan);

        btn = (ElegantNumberButton) findViewById(R.id.btnLaporan1);
        btn = (ElegantNumberButton) findViewById(R.id.btnLaporan2);
        btn = (ElegantNumberButton) findViewById(R.id.btnLaporan3);
        btn = (ElegantNumberButton) findViewById(R.id.btnLaporan4);
        btn = (ElegantNumberButton) findViewById(R.id.btnLaporan5);
        btn = (ElegantNumberButton) findViewById(R.id.btnLaporan6);
        btn = (ElegantNumberButton) findViewById(R.id.btnLaporan7);
        btn = (ElegantNumberButton) findViewById(R.id.btnLaporan8);
        btn = (ElegantNumberButton) findViewById(R.id.btnLaporan9);
        btn = (ElegantNumberButton) findViewById(R.id.btnLaporan10);
        btn = (ElegantNumberButton) findViewById(R.id.btnLaporan11);
        btn = (ElegantNumberButton) findViewById(R.id.btnLaporan12);
        btn = (ElegantNumberButton) findViewById(R.id.btnLaporan13);

        btn.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(View view) {
                String num=btn.getNumber();
                Log.e("NUM",num);
            }
        });

        Button btn1 = (Button) findViewById(R.id.btn_savelaporan);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Data berhasil disimpan" , Toast.LENGTH_SHORT ).show();
            }
        });

    }

    public void savelaporan(View v) {

    }
}
