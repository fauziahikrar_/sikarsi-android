package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;

public class riwayat extends AppCompatActivity implements View.OnClickListener{

    private CardView riwayat1, riwayat2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarRiwayat);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.judulRiwayat);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        riwayat1 = (CardView) findViewById(R.id.riwayat1);
        riwayat2 = (CardView) findViewById(R.id.riwayat2);

        riwayat1.setOnClickListener(this);
        riwayat2.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent i;

        switch (v.getId()) {
            case R.id.riwayat1:
                i = new Intent(this, com.example.myapplication.detailriwayat.class);
                startActivity(i);
                break;
            case R.id.riwayat2:
                i = new Intent(this, com.example.myapplication.detailriwayat.class);
                startActivity(i);
                break;
            default:
                break;
        }

    }
}
