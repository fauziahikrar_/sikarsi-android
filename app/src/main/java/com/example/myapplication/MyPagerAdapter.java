package com.example.myapplication;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class MyPagerAdapter extends FragmentStatePagerAdapter {
    public MyPagerAdapter(FragmentManager fm){

        super(fm);
    }
    @Override

    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new faq();
            case 1:
                return new panduan();
            case 2:
                return new sop();
        }
        return null;
    }
    @Override
    public int getCount() {
        return 3;
    }
    @Override

    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "FAQ";
            case 1:
                return "Panduan";
            case 2:
                return "SOP";
            default:
                return null;
        }
    }

}
