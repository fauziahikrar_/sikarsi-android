package com.example.myapplication;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import static android.app.ProgressDialog.show;

public class MainActivity extends AppCompatActivity {
    private TextInputLayout InputEmail, InputPassword;
    private Button ButtonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View textEmail = findViewById(R.id.input_email);
        View textPassword = findViewById(R.id.input_password);

        ButtonLogin= (Button) findViewById(R.id.btn_login);

        ButtonLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openHomeActivity();
            }

        });

    }

    private void openHomeActivity() {
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }

    private boolean validateEmail(){
        String emailInput = InputEmail.getEditText().getText().toString().trim();

        if (emailInput.isEmpty()){
            InputEmail.setError("Email tidak boleh kosong");
            return  false;
        } else {
            InputEmail.setError((null));
            return true;
        }
    }

    private boolean validatePassword(){
        String passwordInput = InputPassword.getEditText().getText().toString().trim();
        if (passwordInput.isEmpty()){
            InputPassword.setError("Password tidak boleh kosong");
            return  false;
        } else {
            InputPassword.setError((null));
            return true;
        }
    }

    public void logininput(View v){
        if (!validateEmail() | validatePassword()) {
            return;
        }

        Toast.makeText(getBaseContext(), "Login Berhasil!" , Toast.LENGTH_SHORT ).show();
    }


}
